package main;

import java.util.List;
import java.util.stream.Collectors;

public class Ejecutable {
    public static void main(String args[]) {
        List<Integer> numeros = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        List<Integer> result = numeros.stream().map(numero -> numero * numero).collect(Collectors.toList());
        System.out.println("Ejemplo de transformación de valores con Streams.");
        System.out.println("Lista de números original: ");
        System.out.println(numeros);
        System.out.println("Lista de números transformados: ");
        System.out.println(result);
        //
        System.out.println("Ejemplo con foreach: ");
        numeros.stream().map( numero -> numero * numero).forEach( numero -> System.out.println(numero));
    }
}
